# Instal·lació de GNU/Linux Debian 11 (Bullseye) 

**Curs 2021-2022**

* Alerta! Si hi ha una situació diferent a la descrita: aviseu a algun professor.



## Arrencada

Instal·larem el sistema operatiu Debian per xarxa. Per fer-ho ens calen
targetes de xarxa compatibles amb el protocol PXE. 



+ Els ordinadors fan boot de la xarxa utilitzant el protocol PXE. En la arrencada cal prémer F12.

+ Apareix un menú. Escollim com a versió de Debian la primera opció (Debian 11 Bullseye)

## Instal·lació i configuració

Un cop descarregada la imatge apareix la pantalla per triar l’idioma.

**Important**: triem com a idioma d’instal·lació l’**English**.

Ara cal configurar les diferents seccions que ens demanen amb el teclat, fent
servir _les fletxetes_ __←↑↓→__ i el _tabulador_ __↹__


1. _Select your location_:

    Marquem _other_, _Europe_, _Spain_.
    
    Això farà que ens agafi temporalment com a *locale* United States en_US.UTF-8: acceptem ↩

2. _Configure the keyboard_:

    Ens demanen quin teclat farem servir, escollim el català, __Catalan__ que
    ja inclou entre d'altres castellà i anglès.

3. _Configure the Network_:

    Un cop detecti el maquinari de xarxa i el configuri de manera automàgica
    ens demanarà que li posem un nom al nostre ordinador (_hostname_), en
    principi el detectarà. Acceptem o sigui seleccionem `<Continue>`

    Ens detectarà el nom del domini (_Domain name_):

    `informatica.escoladeltreball.org`

    Acceptem o sigui seleccionem `<Continue>`

4. _Choose a mirror of the Debian archive_:

    Aquí se'ns demana que escollim un dels molts ordinadors que contenen una
    rèplica de Debian per baixar-nos d'allà el gruix dels fitxers de la
    instal·lació. 
    
    * Debian archive mirror __country__: seleccionem _Spain_
    * Debian archive mirror: seleccionem l'associació CAtalan LInux Users,  _ftp.caliu.cat_
    * Http Proxy information: ho deixem en blanc.

5. _Set up users and passwords_:

    Password de _root_ i creació usuari _guest_:

    * Entrem _el planeta del sistema solar indicat_ com a password de _root_  (tot en minúscules)  ↩ 
    * Ens demana que tornem a escriure la mateixa paraula de pas.

    Creació d'usuari _guest_ temporal:

    * Ens demana un nom: _guest_
    * Ens demana un username: _guest_
    * Ens demana una contrasenya (posem la pitjor contrasenya del mon): _guest_

6. _Configure the clock_

    Establirem l'hora del sistema:

    Detectarà _Madrid_, acceptem ↩

7. _Partition disks_
    
    + TARDA

        > __Alerta !!! Alerta !!! Alerta !!! Com que se suposa que ja s'han fet les particions, *només utilitzem la nostra partició*__

        Escollim opció d'instal·lació _Manual_

        - Partició del sistema Debian (Tarda). Seleccionem la línia de __#6 Logical ...__ ↩,
            - _Use as_: entrem amb intro ↩ i escollim _Ext4_
            - _Format the partition_: entrem amb intro ↩ i ens canviarà a yes,format it
            - _Mount point_: entrem amb intro ↩ i seleccionem _/ - the root file system_
            - _Label_: entrem amb intro ↩ i escrivim _DEBIAN_TARDA_
            - Al mateix menú seleccionem l'opció que indica que ja tenim configurada aquesta partició: _Done setting up the partition_

        - En principi hauríem de veure que les unitats #6 i #7 tenen en una de les columnes una F (de format).

        - Establim de manera **permanent** aquests canvis: _Finnish partitioning and write changes to disk_

         - Repassem la info i en principi si tot està bé fem definitius els canvis:
         Responem `<Yes>` a la pregunta _Write Changes to disk_

    + MATÍ

    > __Alerta !!! Alerta !!! Alerta !!! Això només es fa el primer dia d'instal·lació, altrament eliminarem el sistema operatiu del company de la tarda!!!__ 

    - Escollim opció d'instal·lació _Manual_

    - Eliminarem totes les particions del nostre dispositiu, creant una taula
      de particions nova. Col·loquem el cursor sobre la línia del dispositiu
      ( _SCSI1_ (0,0,0) (sda)_  o en altres ordinadors _/dev/nvme0n1_ ...) i seleccionem `<Yes>`.

      - Partició del sistema Debian (Mati). Seleccionem la línia de _FREE
        SPACE_ ↩,  creem una nova partició:
        - Mida: _100 GB_ (aula N2J) /  _200 GB_ (aules N2H, N2I, F2A, F2G )
        - Tipus: _Logical_
        - Lloc: _Beginning_
        - Al menú que ens surt afegim l'etiqueta (_Label_): _DEBIAN_MATI_
        - Al mateix menú seleccionem l'opció que indica que ja tenim
          configurada aquesta partició: _Done setting up the partition_
      
      - Partició del sistema Debian (Tarda). Seleccionem la línia de _FREE
        SPACE_ ↩,  creem una nova partició:
        - Mida: el mateix que hem posat a la del Matí
        - Tipus: _Logical_
        - Lloc: _Beginning_
        - Al menú que ens surt afegim l'etiqueta (_Label_): _DEBIAN_TARDA_
        - **Alerta**: _Mount point_ entrem i seleccionem no muntar-la _Do not mount it_
        - Al mateix menú seleccionem l'opció que indica que ja tenim
          configurada aquesta partició: _Done setting up the partition_

      - Partició d'intercanvi _SWAP_ (comuna). Seleccionem la línia de _FREE
        SPACE_ ↩,  creem una nova partició:
        - Mida: _5 GB_
        - Tipus: _Logical_
        - Lloc: _Beginning_
        - Al menú que ens surt seleccionem l'opció _Use as_ per canviar el
          tipus de sistema de fitxers: _swap area_ 
        - Al mateix menú seleccionem l'opció que indica que ja tenim
          configurada aquesta partició: _Done setting up the partition_

      - Establim de manera **permanent** aquests canvis: _Finnish partitioning and write changes to disk_

        Tindrem un avís (Warning) de que hi ha una partició (la de la tarda) a
        la qual no li hem assignat cap punt de muntatge. Seleccionem `<No>` ↩

      - Repassem la info i en principi si tot està bé fem definitius els canvis:
        
        Responem `<Yes>` a la pregunta _Write Changes to disk_.


8. _Configuring popularity-contest_

    Escollim l'opció per defecte `<No>`


9. _Software selection_

    Escollim les opcions que hi ha per defecte

10. _Install the GRUB boot loader_

    Seleccionem `<Yes>` a la pregunta de si volem instal·lar el GRUB al dispositiu primari.

    Escollim `/dev/sda` com a dispositiu a on instal·lar el GRUB

11. _Finnish the installation_

    Seleccionem `<Continue>`

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

**Final instal·lació bàsica**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

Seguim amb les instruccions del fitxer sobre [Configuració](02-Configuracio.md):
caldrà iniciar sessió gràfica com l’usuari _guest_ i obrir el _Firefox_ per poder
consultar el fitxer [Configuració](02-Configuracio.md).

<!-- 
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->

