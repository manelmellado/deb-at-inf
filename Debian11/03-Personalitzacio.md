# Personalització de GNU/Linux Debian 11 (Bullseye)

# Curs 2021-2022

## Punts clau on posar atenció

* Quan posem la nostra contrasenya d’usuari hem de vigilar de no usar el teclat
  **numèric**. També cal verificar de no prèmer la tecla **Bloq Mayús**.

## Personalització

1. Iniciem sessió gràfica amb el nostre propi compte d’usuari.  Verifiquem en un
   terminal l’accés al compte d’usuari modificant la contrasenya. Cal que
   compleixi unes determinades normes de longitud i combinació de lletres i
   xifres.
   
    ```
    $ passwd
    ```

2. Ens posem com a usuari _root_ (amb l’ordre `su -l` ) i instal·lem el servei
`gpm`; i automàticament es configura per estar sempre engegat:

    ```
    $ su -l
    # apt-get -y install gpm
    ```

    Podem també assegurar el compte de convidat (*guest*) amb l’ordre:

    ```
    # apt-get install pwgen -y
    # n=$(pwgen -1 ); echo -e "$n\n$n" | passwd guest ; unset n 
    ```

3.  Repositoris  no lliures.

    Afegirem els repositoris que no són lliures (non-free) o que fan servir components no lliures (contrib)
    
    Per aconseguir això editarem el fitxer que conté la informació dels repositoris:

    ```
    # vim /etc/apt/sources.list
    ```

    I afegirem les paraules _contrib_ i _non-free_ a continuació de _main_
    
    No cal fer-ho a totes les línies, podem obviar les que posi _deb-src_

    Hauria de quedar :

    ```
    deb http://ftp.caliu.cat/debian/ bullseye main contrib non-free
    ...

    deb http://security.debian.org/debian-security bullseye-security main contrib non-free
    ...
    deb http://ftp.caliu.cat/debian/ bullseye-updates main contrib non-free
    ...
    ```
    
    Si has llegit fins aquí, tens premi, aquest apartat es pot fer copiant la seguent instrucció:

    ```
    sed -i '/^[^#]/  s/main$/main contrib non-free/' /etc/apt/sources.list
    ```

4. Programari extra.

    Instal·lem navegador, reproductor multimèdia, editor d'imatges, programari
    de control de versions, diccionaris de català i castellà i els headers del
    kernel necessaris per desenvolupament:

    Primer de tot refresquem l'informació dels repositoris (i més tenint en
    compte que a l'apartat anterior hem afegit els _contrib_ i _non-free_ ):

    ```
    # apt-get update
    ```

    Ara sí, instal·lem:

    ```
    # apt-get install chromium vlc gimp git tig meld linux-headers-`uname -r` gnome-shell-extension-desktop-icons terminator firmware-realtek firmware-misc-nonfree hunspell-ca hunspell-es
    ```

5. Instal·lació d'epoptes.

	Per executar aquesta instrucció haurem de ser root i demanar al professor
quin és el nom de la teva aula.

	Per exemple, si l'aula és a **N2H** l'ordre serà:

	```
	echo 'profeN2H.informatica.escoladeltreball.org server' >> /etc/hosts
	```

	( anàlogament es faria si és N2I o N2J ... )

	Instal·lem els paquets adients:

	```
	apt-get install --install-recommends epoptes-client -y
	```
	
	Tal i com hem fet a la 1a instrucció, si la nostra aula és la **N2H** executem: 
	
	```
	echo SERVER=profeN2H.informatica.escoladeltreball.org >> /etc/default/epoptes
	```

	( anàlogament es faria si és N2I o N2J ... )

	Finalment aconseguim els certificats del server, a la següent ordre potser s'haurà de **canviar _N2H_ pel nom de l'aula adient**:
	```
	epoptes-client -c  profeN2H.informatica.escoladeltreball.org
	```
	Un cop acabem els segúents apartats, reiniciarem l'ordinador perquè epoptes funcioni.

6. Bloqueig del _kernel_ i els _headers_ del _kernel_

	Per bloquejar el kernel actual i que no es pugui actualitzar farem:
	
	```
	apt-mark hold linux-image-$(uname -r)
	``` 	
	I el mateix pels _headers_:
	```
	apt-mark hold linux-headers-$(uname -r)
	```

	Si algun dia volguessim treure aquest bloqueig faríem: servir `apt-mark unhold ...`

7. Servidor gràfic _Xorg_.

    Canviem el _servidor gràfic_ de _Wayland_ a _Xorg_.

    ```
    sed -i 's/\#WaylandEnable=false/WaylandEnable=false/' /etc/gdm3/daemon.conf
    ```

8. Espai en disc.

    Deixem de ser _root_ per tornar a l’usuari personal amb l’ordre:

    ```
    # exit
    ```
   
    Tots els usuaris disposem d'una quota de 100 MB al servidor de departament. Per saber l’espai ocupat i controlar possibles fitxers massa grans podem executar l'ordre:
    ```
    $ du -h --max-depth=1 $HOME/$USERNAME 
    ```

9. Per desactivar les actualitzacions automatiques del sistema en el nostre
   perfil d’usuari cal executar:

    ```
    $ gsettings set org.gnome.software download-updates false
    ```

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

**Final personalització**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * 

## Procediments de verificació

* Funcionament correcte de Kerberos: simplement iniciem sessió en una
  consola  amb el compte personal d’usuari (anem a la consola prement **Ctrl+Alt+F3**).
    
* Els directoris del servidor estan correctament exportats?

    ```
    $ mount | grep gandhi
    ```

* Hi ha programari que no es pot instal·lar, com per exemple *vlc*?

    Comprova que la instal·lació prèvia dels repositoris de *rpmfusion* s'hagi executat correctament.

* Tinc problemes de gràfics amb cert programari (virtualització, etc)

    En aquesta versió de Debian es fa servir per defecte *Wayland*. Nosaltres hem canviat a *Xorg*, assegura't que ho has fet bé. Amb el teu usuari executa a una terminal gràfica l'ordre `echo $XDG_SESSION_TYPE` i que et retorna _x11_.

* No veus les icones a l'escriptori?

    Assegura't que has instal·lat el paquet *gnome-shell-extension-desktop-icons* i després pots configurar-ho des de la pestanya de les extensions a *Tweaks*. O si vols des de la consola (abans però hauràs de llegir aquest parèntesi que diu que és necessari fer *logout*):

    ```
    gnome-extensions enable desktop-icons@csoriano
    ```
* Al mateix Tweaks pots canviar el comportament del botó dret del _touchpad_ (_Keyboard & Mouse_ -> _Area_)

* Si vols deshabilitar el "natural scrolling" perquè ets major que els millenials o perquè t'agraden els vintages ho pots fer des del menú settings que trobes a la cantonada superior dreta de Gnome (Mouse & touchpad)

 
<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->

