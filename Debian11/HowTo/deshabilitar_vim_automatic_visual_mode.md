## Com deshabilitar el visual mode de VIM amb el ratolí

Si no ens interessa aquesta _feature_ la podem deshabilitar pel nostre usuari
afegint (o creant) al nostre usuari `.vimrc` una instrucció:

```
echo "set mouse-=a" >> ~/.vimrc
```

![Acudit sobre la complexitat de Vim](IVeUsingVim.png)


 
