## Com fer que un usuari sigui sudo (Super User DO)


Per poder executar algunes ordres com a administrador, podem fer servir la directiva `sudo` davant de l'ordre en qüestió. Aquesta ordre només la podem fer servir si el nostre usuari es troba de manera implícita (el nom del nostre usuari) o explícita (el nom d'un grup al qual pertany el nostre usuari) al fitxer `/etc/sudoers`.
                       
### Versió simple

+ A Debian existeix un grup que ja es troba a `/etc/sudoers` per poder fer
`sudo` i es diu _sudo_ (hi ha d'altres distribucions com Fedora/RedHat a on el
grup es diu _wheel_).

Afegim el nostre usuari al grup _sudo_. Suposem que el nostre usuari es diu _usuari_, com a _root_ fem:

```
adduser usuari sudo
```

Els grups als quals pertany l'usuari es poden veure amb l'ordre `groups` però
aquesta ordre llegeix la informació quan fem login, per tant fins que no sortim
del nostre usuari i tornem a entrar no veurem els canvis. De vegades hem de fer un
reboot per assegurar-nos que tots els processos del nostre usuari han acabat.

### LINKS
+ [Documentació de Debian](https://wiki.debian.org/sudo)

![acudit xkcd de sudo](sandwich.png)
