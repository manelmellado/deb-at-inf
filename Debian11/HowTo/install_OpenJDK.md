## Instal·lació de versions anteriors d'OpenJDK a Debian


Debian ja té una versió de OpenJDK per defecte instal·lada i alguna altra que es pot cercar als seus repositoris (amb `apt-get`, `aptitude` ...) però podríem necessitar alguna versió que no estigués disponible al repositori de Debian.


### Instal·lació d'OpenJDK 8 a Debian 11


+ Requeriments:
	Es necessita `wget` i  gnupg` però a Debian 11 per defecte ja estan instal·lats.
+ Baixem la clau AdoptOpenJDK:
	```
	wget https://adoptopenjdk.jfrog.io/adoptopenjdk/api/gpg/key/public
	```
+ Fem que la clau sigui accesible per `apt`:
	``` 
	gpg --no-default-keyring --keyring ./adoptopenjdk-keyring.gpg --import public
	gpg --no-default-keyring --keyring ./adoptopenjdk-keyring.gpg --export --output adoptopenjdk-archive-keyring.gpg
	```
	
+  Netegem:
	```
	rm adoptopenjdk-keyring.gpg
	```
+ Guardem l'anell de claus al seu directori:
	```
	mv adoptopenjdk-archive-keyring.gpg /usr/share/keyrings
	```
+ Configurem el repositori apt de _AdoptOpenJDK's_:
	```
	echo "deb [signed-by=/usr/share/keyrings/adoptopenjdk-archive-keyring.gpg] https://adoptopenjdk.jfrog.io/adoptopenjdk/deb bullseye main" | tee /etc/apt/sources.list.d/adoptopenjdk.list
	```
+ Actualitzem els repositoris:
	```
	apt-get update
	```
+ Mostrem totes les variants disponibles d'AdoptOpenJDK:
	```
	apt-cache search adoptopenjdk
	```
+ Instal·lem:
	```
	apt-get install adoptopenjdk-8-openj9 
	```

### Links

+ Aquestes instruccions es basen en l'script més genèric que es troba [aquí](https://gist.github.com/InventivetalentDev/54f5fd5fef3c530cc5614e65ce8500bf) que al seu torn beu de la documentació oficial que es troba [en aquest enllaç](https://adoptopenjdk.net/installation.html?variant=openjdk8&jvmVariant=openj9#linux-pkg)


### Notes

+ No cal instal·lar `apt-transport-https` tal i com diu l'ordre `apt-cache show apt-transport-https`. 
+ No fem servir la instal·lació que utilitza `apt-key`, configurar la seguretat d'un component basant-se en una eina que al seu `man` diu _Deprecated APT key management utility_ no és acceptable.
+ Si al nostre sistema hi hagués més d'una openJDK l'hauríem de gestionar amb [`update-alternatives`](https://wiki.debian.org/DebianAlternatives)

![Acudit sobre l'excés d'us de Java en situacions on no caldria: matar mosques a canonades](golden_hammer.png)
