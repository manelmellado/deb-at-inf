# Configuració de GNU/Linux Debian 11 (Bullseye)

# Curs 2021-2022


## Punts clau on posar atenció

* És obligatori seguir sempre l’ordre de les operacions presentades.

* Si el sistema no ens ofereix iniciar sessió com a _guest_ és que no hem
  creat correctament aquest usuari: per arreglar-ho anem a una consola, creem
  l&rsquo;usuari amb l'ordre `adduser` i reiniciem el sistema amb `reboot`.

## Configuració

1. Iniciem sessió gràfica com l’usuari _guest_. Obrim el _Firefox_ i un terminal;
   disposem les finestres per veure-les simultàniament.

    * En el navegador visualitzem les instruccions en la pàgina de
      **Configuració** dins del repositori de _Git_ mencionat més amunt.
    * En el terminal ens convertim en administrador executant `su -l`.

    Notació: a partir d’ara si veiem el caràcter `#` davant d’una ordre que hem
    d’executar voldrà dir que per escriure-la haurem de ser l’usuari _root_ (el _super
    usuari_ o administrador). I aquest caràcter `#` no l’hem de posar.

2. Instal·lació d'editors i gestors de fitxers i altres paquets.

    En comptes de fer servir l’editor vi farem servir vim (life is colorful!).

    L’instal·lem com a _root_:

    ```
    # apt-get -y install vim vim-gtk3 mc geany aptitude tree openssh-server && systemctl disable ssh
    ```


3. Configuracions pròpies del departament d'Informàtica.

    Optimització del sistema (com a root):
    
    ```
    # apt-get purge gnome-games -y && apt-get autoremove -y
    ```

    Instal·lació de dependències:

    ```
    # apt-get install krb5-user krb5-multidev libpam-mount sssd nfs-common autofs ufw curl -y 
    ```  

    _Acceptem TOTES les preguntes posteriors que ens farà el procés de configuració. O sigui, NO ESCRIVIM RESº_


    Descarreguem l’arxiu de configuracions i el descomprimim (com a _root_):

    ```
    # cd /tmp
    # wget ftp://gandhi/pub/debian-deploy/config.tgz
    # tar --directory=/ --no-same-owner -xvzf config.tgz
    ```

    Gestionem alguns serveis [in]necessaris:
    
    ```
    # systemctl disable sssd-nss.socket   	
    # systemctl disable sssd-autofs.socket
    # systemctl restart sssd
    # systemctl restart rpc-gssd
    ```

4. Edició del fitxer `/etc/fstab`.

    Editem el fitxer /etc/fstab amb vim:
    ```
    # vim /etc/fstab
    ```
    Premem la tecla **i** per inserir contingut en el fitxer.

    Editem el fitxer `/etc/fstab` i substituïm els `UUID` o `LABEL` de la
    partició arrel ( _/_ ) i de la partició _swap_. Posar-hi en el seu lloc el *device*
    pertinent: `/dev/sda5` per la partició arrel del matí o `/dev/sda6` per la
    partició arrel de la tarda,  i `/dev/sda7` per a la de _swap_.

    On diu, per exemple:

    ```
    UUID=236896b6-5701-4411-a2e8-96442c3d4725	/	ext4	errors=remount-ro	0 1
    UUID=151174f5-5652-4b02-af4b-942f2a70d31a	none	swap	sw			0 0
    ```

    Ha de dir, en el cas del matí:

    ```
    /dev/sda5	/	ext4	errors=remount-ro	0 1 #/dev/sda6 a la TARDA
    /dev/sda7  	none	swap	sw			0 0

    ```

    A continuació afegim la línia següent al final de tot:

    ```
    gandhi:/groups/ /home/groups  nfs4  sec=krb5,comment=systemd.automount  0  0
    ```
    Per desar cal prémer la tecla **ESC** per entrar en mode ordre i llavors escriure **:wq**.
    
    Fem un test per comprovar si la sintaxi és correcta però no muntem el nou sistema de fitxers muntat els sistemes de fitxers fem:

    ```
    # mount -fav
    ```

    Si hi ha errors serà perquè alguna línia no s'ha escrit bé.

8. Ara cal reiniciar el sistema executant en el terminal l’ordre `reboot`. **Mai** s’apaga un sistema Linux directament
   (cal usar `poweroff`, `shutdown` o `reboot`).

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

**Final configuració**

* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

Seguim amb les instruccions del fitxer sobre [Personalització](03-Personalitzacio.md):
caldrà iniciar sessió gràfica amb el teu usuari i obrir el _Firefox_ per poder
consultar el fitxer [Personalització](03-Personalitzacio.md).

<!--
vim:ts=4:sw=4:ai:et:fileencoding=utf8:syntax=markdown
-->
